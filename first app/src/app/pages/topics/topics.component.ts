import { Component } from '@angular/core';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-topics',
  templateUrl: './topics.component.html',
  styleUrls: ['./topics.component.css']
})
export class TopicsComponent {
  stringInterpolation = 'Data binding example using String Interpolation {{ Like This }}';

  oneWayBinding = 'This is the one way binding';
  // Properties to be bound
  message: string = 'one way binding';
  username: string = 'JohnDoe';

  data = "Ram and Syam"; 
person = {"country":"UK"};
}
