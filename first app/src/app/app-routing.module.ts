import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HeroesComponent } from './heroes/heroes.component'; 
import { AboutComponent } from './pages/about/about.component';
import { ContactsComponent } from './pages/contacts/contacts.component';
import { TopicsComponent } from './pages/topics/topics.component';



const routes: Routes = [
  { path: 'heroes', component: HeroesComponent },
  {path:'About',component: AboutComponent},
{path:'Contacts',component:ContactsComponent},
{path:'Topics',component:TopicsComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
